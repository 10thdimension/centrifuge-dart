This is an updated version of [centirfuge-dart](https://pub.dev/packages/centrifuge)

## [0.0.2] - (26/02/2021)

- Updated WebSocket package
## [0.0.1] - (26/02/2021)

- The packages have been updated
- Protoc file udpated with version libprotoc 3.14.0 and Dart protoc_plugin 20.0.0
- Added web_socket_channel: ^1.1.0 for communication over Websocket
- Added compatibility for Web and Mobile using web_socket_channel package
- Removed deprecated implementation
- Known issues:
  - Ping at interval not added
