# Centrifuge Dart / Flutter SDK

Dart client to communicate with Centrifuge and Centrifugo from Flutter and VM over web_socket_channel

Updated Dart SDK (using Web Socket Channel - https://pub.dev/packages/web_socket_channel) implementation of the original (https://github.com/centrifugal/centrifuge-dart) along with compatibility for Web

**Known fixes to be added:**

- Ping at intervals
